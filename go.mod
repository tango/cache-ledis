module gitea.com/tango/cache-ledis

go 1.14

require (
	gitea.com/lunny/tango v0.6.2
	gitea.com/tango/cache v0.0.0-20200330032101-c35235184e65
	github.com/ledisdb/ledisdb v0.0.0-20200510135210-d35789ec47e6
	github.com/smartystreets/goconvey v1.6.4
	github.com/unknwon/com v1.0.1
	gopkg.in/ini.v1 v1.57.0
)
